import { Routes } from '@angular/router'
import { PropertiesComponent } from './properties/properties.component';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { AuthGuard } from './api/client/auth/auth.guard';
import { PropertyComponent } from './property/property.component';
import { CreatePropertyComponent } from './create-property/create-property.component';

export const appRoutes: Routes = [
    { path: 'home', component: PropertiesComponent, canActivate: [AuthGuard] },
    { path: 'properties/:propertyId', component: PropertyComponent },
    { path: 'create', component: CreatePropertyComponent },
    {
        path: 'signup', component: UserComponent,
        children: [{ path: '', component: SignUpComponent }]
    },
    {
        path: 'login', component: UserComponent,
        children: [{ path: '', component: SignInComponent }]
    },
    { path: '', redirectTo: '/home', pathMatch: 'full' }

];