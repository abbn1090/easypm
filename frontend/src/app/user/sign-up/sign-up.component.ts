import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../api/client/user/user.model';
import { UserService } from '../../api/client/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {
  user: User;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
    this.user = {
      name: 'test',
      password: '12345',
      email: 'test@gmail.com'
    };
  }

  OnSubmit(form: NgForm) {
    this.userService.registerUser(form.value)
      .subscribe((data: any) => {
        if (data && data._id) {
          this.resetForm(form);
          this.toastr.success('User registration successful');
          this.router.navigate(['/login']);
        } else {
          this.toastr.success('Something went wrong');
        }
      },
        (err: HttpErrorResponse) => {
          console.log("err", JSON.stringify(err));
        });
  }

}

