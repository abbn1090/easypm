import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PropertiesComponent } from './properties/properties.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserComponent } from './user/user.component';

import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { UserService } from './api/client/user/user.service';
import { AuthGuard } from './api/client/auth/auth.guard';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './api/client/auth/auth.interceptor';
import { FormsModule } from '@angular/forms';
import { PropertyService } from './api/client/properties/property.service';
import { ToastrModule } from 'ngx-toastr';
import { PropertyComponent } from './property/property.component';
import { CreatePropertyComponent } from './create-property/create-property.component';


@NgModule({
  declarations: [
    AppComponent,
    PropertiesComponent,
    SignInComponent,
    SignUpComponent,
    UserComponent,
    PropertyComponent,
    CreatePropertyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService, PropertyService, AuthGuard,
    ,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
