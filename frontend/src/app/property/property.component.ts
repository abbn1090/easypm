import { Component, OnInit } from '@angular/core';
import { PropertyService, Property } from '../api/client/properties/property.service';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {
  property: Property;
  constructor(private propertyService: PropertyService) { }

  ngOnInit() {
    this.getProperty();
  }

  getProperty() {
    this.propertyService.getProperty("z123")
      .subscribe(property => {

        this.property = property;
        console.log("this.property :", this.property);
      });
  }
}
