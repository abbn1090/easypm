import { Component, OnInit } from '@angular/core';
import { Property, PropertyService } from '../api/client/properties/property.service';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {
  title = 'Easy Property Management';

  properties: Property[] = [];
  constructor(private propertyService: PropertyService) { console.log('called properties'); }


  ngOnInit(): void {
    this.loadProperties();
  }

  loadProperties() {
    this.propertyService.queryProperties()
      .subscribe(properties => {
        this.properties = properties;
      });
  }

}
