const Joi = require("joi");

import * as express from 'express';

import { PropertyService } from './property.service';

import { Property } from './property.model';

const propertyService = new PropertyService();

const controller = express.Router();

controller.post('/', async (req, res) => {
  const query = req.body;
  const properties = await propertyService.listProperties(query, req.query.offset, req.query.limit);
  res.send(properties);
});
controller.post('/create/', async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  let property = req.body;
  property = await propertyService.create(property);
  res.send(property);
});
controller.get('/:id', async (req, res) => {
  const id = req.params.id;
  const property = await propertyService.getProperty(id);
  if (!property)
    return res.status(404).send("The property with the given ID was not found.");
  res.send(property);
});
function validate(property) {
  const schema = {
    name: Joi.string()
      .min(2)
      .max(50)
      .required(),
    address: Joi.string()
      .min(5)
      .max(255)
      .required()
  };

  return Joi.validate(property, schema);
}
export { controller as PropertyController };
