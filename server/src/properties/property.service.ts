import { PropertyDAO } from './property.dao';
import { Property } from './property.model';

export class PropertyService {

  constructor(
    private dao = new PropertyDAO()
  ) { }

  public listProperties(
    query: any = {},
    offset: number = 0,
    limit: number = 10
  ): Promise<Property[]> {
    console.log("called list prp")
    return this.dao.query(query, offset, limit);
  }
  public getProperty(
    _id: string
  ): Promise<Property> {
    return this.dao.getProperty(_id);
  }
  public create(
    property: Property
  ): Promise<string> {
    return this.dao.insert(property);
  }

}
