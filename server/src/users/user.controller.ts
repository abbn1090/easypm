import * as express from 'express';

import { UserService } from './user.service';

const userService = new UserService();

const controller = express.Router();
const bcrypt = require("bcrypt");
const _ = require("lodash");
const { User, validate, generateAuthToken } = require("./user.model");


controller.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await userService.findUserByEmail(req.body.email);
  if (user) return res.status(400).send("User already registered.");

  user = new User(_.pick(req.body, ["name", "email", "password"]));
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);
  let id_user = await userService.register(user);

  const token = generateAuthToken(user, id_user);
  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email"]));
});

export { controller as UserController };
