const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");

export class User {
  _id?: string; // Assigned automatically by datastore
  name: string;
  email: string;
  password: string;
  constructor(data) {
    this.name = data.name;
    this.email = data.email;
    this.password = data.password;
  }
}
function generateAuthToken(user, _id) {
  const token = jwt.sign(
    {
      _id: _id,
      name: user.name,
      email: user.email
    },
    config.get("jwtPrivateKey")
  );
  return token;
};
function validateUser(user) {
  const schema = {
    name: Joi.string()
      .min(2)
      .max(50)
      .required(),
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required()
  };

  return Joi.validate(user, schema);
}
exports.validate = validateUser;
exports.generateAuthToken = generateAuthToken;


