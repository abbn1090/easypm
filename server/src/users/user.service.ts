import { UserDAO } from './user.dao';
import { User } from './user.model';

export class UserService {

  constructor(
    private dao = new UserDAO()
  ) { }

  public findUser(
    id: string
  ): Promise<User> {
    return this.dao.getUser(id);
  }
  public findUserByEmail(
    email: string
  ): Promise<User> {
    return this.dao.queryOne({ "email": email });
  }
  public register(
    user: User
  ): Promise<string> {
    return this.dao.insert(user);
  }

}
